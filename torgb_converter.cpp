#include <iostream>
#include <fstream>
#include <iterator>
#include <string>
#include <algorithm>
#include <limits>
#include <vector>
#include <cstdio>
#include <cinttypes>
#include <array>

extern "C" {
#include <dirent.h>
#include <unistd.h>
}

const std::string data_marker("LOOKUP_TABLE default\n");

bool data_search_on_file(std::istream &f, const std::string &marker)
{
	const std::istreambuf_iterator<char> eof;

	auto ret = std::search(std::istreambuf_iterator<char>(f), eof, marker.begin(), marker.end());
	return ret != eof;
}

typedef std::pair<size_t, size_t> ipair;
typedef std::pair<float, float> fpair;

struct MeshInfo
{
	size_t v_size;
	size_t h_size;
	float v_step;
	float h_step;
};

struct Rectangle
{
	Rectangle() {
		x_min = y_min = std::numeric_limits<size_t>::max();
		x_max = y_max = std::numeric_limits<size_t>::min();
	}
	size_t x_min, x_max;
	size_t y_min, y_max;
};

std::vector<fpair> get_info(const char *dirname, const MeshInfo &minfo, std::vector<Rectangle> &split_limits, 
	unsigned int &dims, unsigned int &splits, unsigned int &frames)
{
	const float inf = std::numeric_limits<float>::infinity();
	std::vector<fpair> limits(2, fpair(inf, -inf));

	DIR* dir = opendir(dirname);
	struct dirent *entry = readdir(dir);

	/* Just get how many dimensions, frames and splits we are deling with.
	 * Assumes there are no gaps. */
	dims = splits = frames = 0;
	while(entry) {
		char dim;
		unsigned int split, frame;
		int ret = std::sscanf(entry->d_name, "mesh_%c_%u-%u.vtk", &dim, &split, &frame);
		if(ret == 3) {
			splits = std::max(splits, split);
			frames = std::max(frames, frame);
			unsigned int ndim = dim - 'u';
			dims = std::max(dims, ndim);
		}
		entry = readdir(dir);
	}
	splits++;
	frames++;
	dims++;
	std::cout << "dimensions: " << dims << ", splits: " << splits << ", frames: " << frames << '\n';
	closedir(dir);

	/* Grab the geometrical limits of each slice.
	 * Assumes it is the same for all files. */
	split_limits = std::vector<Rectangle>(splits);
	for(unsigned int split = 0; split < splits; ++split) {
		char fname[150];
		snprintf(fname, 150, "%s/mesh_u_%u-0.vtk", dirname, split);
		std::ifstream file(fname);
		const std::string marker("POINTS ");
		data_search_on_file(file, marker);

		float xmin = inf, xmax = -inf, ymin = inf, ymax = -inf;
		size_t num_pts;
		file >> num_pts;
		{
			std::string ignore;
			file >> ignore;
			//assert(ignore == "float");
		}
		for(size_t i = 0; i < num_pts; ++i) {
			float x, y, z;
			file >> x >> y >> z;
			xmin = std::min(xmin, x);
			xmax = std::max(xmax, x);
			ymin = std::min(ymin, y);
			ymax = std::max(ymax, y);
		}
		Rectangle &r = split_limits[split];
		r.x_min = std::roundf(xmin / minfo.h_step);
		size_t xcount = std::roundf((xmax - xmin) / minfo.h_step);
		r.x_max = xcount + r.x_min - 1;

		r.y_min = std::roundf(ymin / minfo.v_step);
		size_t ycount = std::roundf((ymax - ymin) / minfo.v_step);
		r.y_max = ycount + r.y_min - 1;

		std::cout << "Split " << split << ":\n  x: " << r.x_min << " --- " << r.x_max
			<< "\n  y: " << r.y_min << " --- " << r.y_max << '\n';
	}

	/* Grab the minimum and maximum value for each channel, so it can be normalized. */
	for(unsigned int frame = 0; frame < frames; ++frame) {
		for(char dim = 'u'; dim < (dims + 'u'); ++dim) {
			unsigned int ndim = dim - 'u';
			for(unsigned int split = 0; split < splits; ++split) {
				char fname[150];
				snprintf(fname, 150, "%s/mesh_%c_%u-%u.vtk", dirname, dim, split, frame);
				std::ifstream file(fname);

				if(!data_search_on_file(file, data_marker))
					continue;

				for(;;) {
					float val;
					file >> val;
					if(file.eof())
						break;
					limits[ndim].first = std::min(limits[ndim].first, val);
					limits[ndim].second = std::max(limits[ndim].second, val);
				}
			}
		}
	}

	std::cerr << "u limits: " << limits[0].first << " ---- " << limits[0].second
		<< "\nv limits: " << limits[1].first << " ---- " << limits[1].second << '\n';

	return limits;
}

template<class T>
class Matrix2
{
public:
	Matrix2(size_t h_size, size_t v_size):
		hs(h_size), vs(v_size),
		vec(h_size * v_size)
	{}

	T& operator()(size_t x, size_t y)
	{
		return vec[y*hs + x];
	}

private:
	size_t hs, vs;
	std::vector<T> vec;
};

typedef std::array<uint8_t, 3> RGB;

int main(int argc, char **argv)
{
	if(argc < 3) {
		std::cerr << "Missing arguments:\n  $ "<< argv[0] << " input_dir width [height]\n";
		return 1;
	}

	const char *dirname = argv[1];
	MeshInfo minfo;
	std::vector<Rectangle> split_limits;

	{
		size_t x, y;
		x = atoi(argv[2]);
		if(argc > 3)
			y = atoi(argv[4]);
		else
			y = x;

		minfo = {x, y, 1.0f/x, 1.0f/y};
	}

	unsigned int dims, splits, frames;
	std::vector<fpair> dim_limits = get_info(dirname, minfo, split_limits, dims, splits, frames);

	Matrix2<RGB> rgb(minfo.h_size, minfo.v_size);

	/*system("mkfifo aaa.rgb");
	if(fork() == 0) {
		char cmd[200];
		snprintf(cmd, 200, "x264 --input-res %lux%lu --crf 18 --input-csp rgb --output-csp i444 --fps 16 --open-gop -o out.mkv aaa.rgb --preset placebo", minfo.h_size, minfo.v_size);
		std::cout << "Command: " << cmd << std::endl;
		system(cmd);
	}*/
	std::ofstream output("aaa.rgb");

	for(unsigned int frame = 0; frame < frames; ++frame) {
		for(char dim = 'u'; dim < (dims + 'u'); ++dim) {
			unsigned int ndim = dim - 'u';
			float dim_offset = -dim_limits[ndim].first;
			float dim_scale = ((2 << 8) - 1) / (dim_limits[ndim].second - dim_limits[ndim].first);
			for(unsigned int split = 0; split < splits; split++) {
				std::ifstream file;
				{
					char fname[150];
					snprintf(fname, 150, "%s/mesh_%c_%u-%u.vtk", dirname, dim, split, frame);
					file.open(fname);
				}
				Rectangle &limits = split_limits[split];
				size_t width = limits.y_max - limits.y_min + 1;

				data_search_on_file(file, data_marker);

				size_t n;
				for(n = 0;; ++n) {
					float val;
					file >> val;
					if(file.eof())
						break;

					size_t x = limits.x_min + n / width;
					size_t y = limits.y_min + n % width;
					rgb(x, y)[ndim] = roundf((val + dim_offset) * dim_scale);
					rgb(x, y)[2] = 0; // suboptimal...
				}
			}
		}

		output.write((char*)&rgb(0,0)[0], minfo.h_size * minfo.v_size * 3);

		/*char fname[150];
		snprintf(fname, 150, "frame_%04u.pnm", frame);
		std::ofstream out(fname);
		out << "P3\n" << minfo.h_size << ' ' << minfo.v_size << " 255\n";
		for(int y = minfo.v_size - 1; y >= 0; --y) {
			for(int x = 0; x < minfo.h_size; ++x) {
				for(int d = 0; d < 2; ++d) {
					out << (unsigned int)rgb(x, y)[d] << ' ';
				}
				out << "0 ";
			}
			out << '\n';
		}
		out.close();*/
	}
	//unlink("aaa.rgb");
}
